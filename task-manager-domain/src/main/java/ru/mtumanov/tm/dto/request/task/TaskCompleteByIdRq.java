package ru.mtumanov.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.request.AbstractUserRq;

@Getter
@Setter
@NoArgsConstructor
public class TaskCompleteByIdRq extends AbstractUserRq {

    @Nullable
    private String id;

    public TaskCompleteByIdRq(@Nullable final String token, @Nullable final String id) {
        super(token);
        this.id = id;
    }

}
