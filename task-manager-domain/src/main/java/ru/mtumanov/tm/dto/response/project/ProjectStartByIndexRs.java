package ru.mtumanov.tm.dto.response.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.model.ProjectDTO;

@NoArgsConstructor
public class ProjectStartByIndexRs extends AbstractProjectRs {

    public ProjectStartByIndexRs(@Nullable final ProjectDTO project) {
        super(project);
    }

    public ProjectStartByIndexRs(@NotNull final Throwable err) {
        super(err);
    }

}
