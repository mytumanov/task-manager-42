package ru.mtumanov.tm.dto.response.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.model.TaskDTO;

@NoArgsConstructor
public class TaskCompleteByIndexRs extends AbstractTaskRs {

    public TaskCompleteByIndexRs(@Nullable final TaskDTO task) {
        super(task);
    }

    public TaskCompleteByIndexRs(@NotNull final Throwable err) {
        super(err);
    }

}
