package ru.mtumanov.tm.dto.response.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.model.ProjectDTO;

@NoArgsConstructor
public final class ProjectRemoveByIndexRs extends AbstractProjectRs {

    public ProjectRemoveByIndexRs(@Nullable final ProjectDTO project) {
        super(project);
    }

    public ProjectRemoveByIndexRs(@Nullable final Throwable err) {
        super(err);
    }

}