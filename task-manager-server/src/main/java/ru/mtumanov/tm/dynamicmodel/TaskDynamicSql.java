package ru.mtumanov.tm.dynamicmodel;

import org.jetbrains.annotations.NotNull;
import org.mybatis.dynamic.sql.SqlColumn;

import java.util.Date;

public final class TaskDynamicSql {

    @NotNull
    public static final TaskTable taskTable = new TaskTable();

    @NotNull
    public static final SqlColumn<String> id = taskTable.id;

    @NotNull
    public static final SqlColumn<Date> created = taskTable.created;

    @NotNull
    public static final SqlColumn<String> description = taskTable.description;

    @NotNull
    public static final SqlColumn<String> status = taskTable.status;

    @NotNull
    public static final SqlColumn<String> tasktUserId = taskTable.userId;

    @NotNull
    public static final SqlColumn<String> name = taskTable.name;

    @NotNull
    public static final SqlColumn<String> projectId = taskTable.projectId;

}
