package ru.mtumanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.api.endpoint.ISystemEndpoint;
import ru.mtumanov.tm.api.service.IPropertyService;
import ru.mtumanov.tm.api.service.IServiceLocator;
import ru.mtumanov.tm.dto.request.server.ServerAboutRq;
import ru.mtumanov.tm.dto.request.server.ServerVersionRq;
import ru.mtumanov.tm.dto.response.server.ServerAboutRs;
import ru.mtumanov.tm.dto.response.server.ServerVersionRs;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.mtumanov.tm.api.endpoint.ISystemEndpoint")
public final class SystemEndpoint implements ISystemEndpoint {

    @NotNull
    private IServiceLocator serviceLocator;

    public SystemEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @NotNull
    @WebMethod
    public ServerAboutRs getAbout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerAboutRq request
    ) {
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final ServerAboutRs serverAboutRs = new ServerAboutRs();
        serverAboutRs.setEmail(propertyService.getAuthorEmail());
        serverAboutRs.setName(propertyService.getAuthorName());
        return serverAboutRs;
    }

    @Override
    @NotNull
    @WebMethod
    public ServerVersionRs getVersion(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerVersionRq request
    ) {
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final ServerVersionRs serverVersionRs = new ServerVersionRs();
        serverVersionRs.setVersion(propertyService.getApplicationVersion());
        return serverVersionRs;
    }

}
