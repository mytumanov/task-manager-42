package ru.mtumanov.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.mybatis.dynamic.sql.SortSpecification;
import org.mybatis.dynamic.sql.render.RenderingStrategies;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import ru.mtumanov.tm.api.repository.IProjectRepository;
import ru.mtumanov.tm.api.service.IConnectionService;
import ru.mtumanov.tm.api.service.IProjectService;
import ru.mtumanov.tm.comparator.CreatedComparator;
import ru.mtumanov.tm.comparator.NameComparator;
import ru.mtumanov.tm.comparator.StatusComparator;
import ru.mtumanov.tm.dto.model.ProjectDTO;
import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.entity.EntityEmptyException;
import ru.mtumanov.tm.exception.field.DescriptionEmptyException;
import ru.mtumanov.tm.exception.field.IdEmptyException;
import ru.mtumanov.tm.exception.field.NameEmptyException;
import ru.mtumanov.tm.exception.user.UserIdEmptyException;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

import static org.mybatis.dynamic.sql.SqlBuilder.isEqualTo;
import static org.mybatis.dynamic.sql.SqlBuilder.select;
import static ru.mtumanov.tm.dynamicmodel.ProjectDynamicSql.*;

public class ProjectService implements IProjectService {

    @NotNull
    final IConnectionService connectionService;

    public ProjectService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @Nullable
    private SortSpecification getComporator(@NotNull final Comparator comparator) {
        if (comparator instanceof CreatedComparator)
            return created;
        else if (comparator instanceof NameComparator)
            return name;
        else if (comparator instanceof StatusComparator)
            return status;
        else
            return null;
    }

    @Override
    @NotNull
    public ProjectDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (name == null || name.isEmpty())
            throw new NameEmptyException();
        if (userId == null || userId.isEmpty())
            throw new UserIdEmptyException();
        if (description == null)
            throw new DescriptionEmptyException();
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        add(userId, project);
        return project;
    }

    @Override
    @NotNull
    public ProjectDTO updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (name == null || name.isEmpty())
            throw new NameEmptyException();
        if (userId == null || userId.isEmpty())
            throw new UserIdEmptyException();
        if (description == null)
            throw new DescriptionEmptyException();
        if (id == null || id.isEmpty())
            throw new IdEmptyException();
        @NotNull final ProjectDTO project = findOneById(userId, id);
        project.setName(name);
        project.setDescription(description);
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            try {
                @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
                repository.update(project);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
        return project;
    }

    @Override
    @NotNull
    public ProjectDTO changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws AbstractException {
        if (id == null || id.isEmpty())
            throw new IdEmptyException();
        if (userId == null || userId.isEmpty())
            throw new UserIdEmptyException();
        @NotNull final ProjectDTO project = findOneById(userId, id);
        project.setStatus(status);
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            try {
                @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
                repository.update(project);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
            }
        }
        return project;
    }

    @Override
    @NotNull
    public List<ProjectDTO> findAll(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty())
            throw new UserIdEmptyException();
        @NotNull List<ProjectDTO> projects;
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            projects = repository.findAllUserId(userId);
        }
        return projects;
    }

    @Override
    @NotNull
    public ProjectDTO add(@Nullable final String userId, @NotNull final ProjectDTO model) throws AbstractException {
        if (userId == null || userId.isEmpty())
            throw new UserIdEmptyException();
        model.setUserId(userId);
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            try {
                @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
                repository.add(model);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
            }
        }
        return model;
    }

    @Override
    @NotNull
    public List<ProjectDTO> findAll(@Nullable final String userId, @Nullable final Comparator<ProjectDTO> comparator)
            throws AbstractException, SQLException {
        if (userId == null || userId.isEmpty())
            throw new UserIdEmptyException();
        if (comparator == null)
            return findAll(userId);
        if (getComporator(comparator) == null)
            return findAll(userId);
        @NotNull final List<ProjectDTO> projects;
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            @NotNull final SelectStatementProvider selectStatement = select(id, created, name, description, projectUserId, status)
                    .from(projectTable)
                    .where(projectUserId, isEqualTo(userId))
                    .orderBy(getComporator(comparator))
                    .build()
                    .render(RenderingStrategies.MYBATIS3);
            projects = repository.findAllComporator(selectStatement);
        }
        return projects;
    }

    @Override
    @NotNull
    public ProjectDTO findOneById(@Nullable final String userId, @Nullable final String id) throws AbstractException {
        if (userId == null || userId.isEmpty())
            throw new UserIdEmptyException();
        if (id == null || id.isEmpty())
            throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            return repository.findOneById(userId, id);
        }
    }

    @Override
    @NotNull
    public void remove(@Nullable final String userId, @Nullable final ProjectDTO model) throws AbstractException {
        if (userId == null || userId.isEmpty())
            throw new UserIdEmptyException();
        if (model == null)
            throw new EntityEmptyException(ProjectDTO.class.getName());
        removeById(userId, model.getId());
    }

    @Override
    @NotNull
    public void removeById(@Nullable final String userId, @Nullable final String id) throws AbstractException {
        if (userId == null || userId.isEmpty())
            throw new UserIdEmptyException();
        if (id == null || id.isEmpty())
            throw new IdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            try {
                @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
                repository.removeById(userId, id);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
    }

    @Override
    public void clear(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty())
            throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            try {
                @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
                repository.clear(userId);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
    }

    @Override
    public int getSize(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty())
            throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            return repository.getSize(userId);
        }
    }

    @Override
    public boolean existById(@Nullable final String userId, @Nullable final String id) throws AbstractException {
        if (userId == null || userId.isEmpty())
            throw new UserIdEmptyException();
        if (id == null || id.isEmpty())
            throw new IdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            return repository.existById(userId, id);
        }
    }

    @Override
    @NotNull
    public List<ProjectDTO> findAll() {
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            return repository.findAll();
        }
    }

    @Override
    @NotNull
    public Collection<ProjectDTO> set(@NotNull Collection<ProjectDTO> models) throws AbstractException {
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            try {
                repository.clearAll();
                for (@NotNull final ProjectDTO project : models)
                    repository.add(project);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
        return models;
    }

}
