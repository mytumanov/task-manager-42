package ru.mtumanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.model.TaskDTO;
import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.exception.AbstractException;

import java.sql.SQLException;
import java.util.List;

public interface ITaskService extends IUserOwnerService<TaskDTO> {

    @NotNull
    List<TaskDTO> findAllByProjectId(@NotNull String userId, @NotNull String projectId) throws AbstractException, SQLException;

    @NotNull
    TaskDTO create(@NotNull String userId, @NotNull String name, @NotNull String description) throws AbstractException, SQLException;

    @NotNull
    TaskDTO updateById(@NotNull String userId, @NotNull String id, @NotNull String name, @NotNull String description) throws AbstractException, SQLException;

    @NotNull
    TaskDTO changeTaskStatusById(@NotNull String userId, @NotNull String id, @NotNull Status status) throws AbstractException, SQLException;

}
