package ru.mtumanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.model.SessionDTO;
import ru.mtumanov.tm.exception.AbstractException;

import java.util.List;

public interface ISessionService {

    @NotNull
    SessionDTO add(@Nullable String userId, @Nullable SessionDTO model) throws AbstractException;

    @NotNull
    List<SessionDTO> findAll();

    @NotNull
    List<SessionDTO> findAll(@Nullable String userId) throws AbstractException;

    @NotNull
    SessionDTO findOneById(@Nullable String userId, @Nullable String id) throws AbstractException;

    @NotNull
    void remove(@Nullable SessionDTO model) throws AbstractException;

    @NotNull
    void removeById(@Nullable String userId, @Nullable String id) throws AbstractException;

}
