package ru.mtumanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.model.ProjectDTO;
import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.exception.AbstractException;

import java.sql.SQLException;

public interface IProjectService extends IUserOwnerService<ProjectDTO> {

    @NotNull
    ProjectDTO create(@Nullable String userId, @Nullable String name, @Nullable String description) throws AbstractException, SQLException;

    @NotNull
    ProjectDTO updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description) throws AbstractException, SQLException;

    @NotNull
    ProjectDTO changeProjectStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status) throws AbstractException, SQLException;

}
