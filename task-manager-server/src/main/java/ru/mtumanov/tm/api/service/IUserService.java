package ru.mtumanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.model.UserDTO;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.AbstractException;

public interface IUserService extends IService<UserDTO> {

    @NotNull
    UserDTO create(@NotNull String login, @NotNull String password) throws AbstractException;

    @NotNull
    UserDTO create(@NotNull String login, @NotNull String password, @NotNull String email) throws AbstractException;

    @NotNull
    UserDTO create(@NotNull String login, @NotNull String password, @NotNull Role role) throws AbstractException;

    @NotNull
    UserDTO findByLogin(@NotNull String login) throws AbstractException;

    @NotNull
    UserDTO findByEmail(@NotNull String email) throws AbstractException;

    @NotNull
    UserDTO findById(@NotNull String id) throws AbstractException;

    @NotNull
    UserDTO removeByLogin(@NotNull String login) throws AbstractException;

    @NotNull
    UserDTO removeByEmail(@NotNull String email) throws AbstractException;

    @NotNull
    UserDTO setPassword(@NotNull String id, @NotNull String password) throws AbstractException;

    @NotNull
    UserDTO userUpdate(@NotNull String id, @NotNull String firstName, @NotNull String lastName, @NotNull String middleName) throws AbstractException;

    boolean isLoginExist(@NotNull String login);

    boolean isEmailExist(@NotNull String email);

    void lockUserByLogin(@NotNull String login) throws AbstractException;

    void unlockUserByLogin(@NotNull String login) throws AbstractException;

}